# JahemTool

#### 项目介绍
个人常用工具

#### 软件架构
软件架构说明

#### 安装教程

1. composer require jahem/jahem-tool-sdk

#### 使用说明

1. Tool 普通常用方法
2. Mysqli DB类
3. Algorithm 计算大数据的方法
4. Uuid uuid类
5. ImgCompress 图片压缩类
6. CUtf8_PY 中文转拼音
7. Crypt 加密类

